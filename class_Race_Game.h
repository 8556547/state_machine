#ifndef MAIN_MENU_H
#define MAIN_MENU_H

class GameFSM
{
public:
	enum EState
	{
		MAINMENU = 0,
		PLAY,
		STATISTICS,
		HISTORY,
		EXIT,
		DEFAULT
	};
	GameFSM()
		:mystate_(MAINMENU) {}


	void SetState(EState state)
	{
		mystate_ = state;
	}
	EState GetState()
	{
		return mystate_;
	}
private:
	EState mystate_;
};

class MainMenu
{
public:
	MainMenu();
	~MainMenu();
	void Beginscreen();
	void Handle();
	void MainSceen();
	void SecondSceen();
	void Play();
	void clearscreen();
private:
	GameFSM my_fsm;
	int myinput_;
};
#endif //MAIN_MENU_H