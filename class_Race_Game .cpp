#include "class_Race_Game.h"
#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <ctime>
#include <Windows.h>


MainMenu::MainMenu()
	: my_fsm(),
	myinput_(0)
{
	std::cout << "MainManu::MainManu \n ";
	Beginscreen();
	Handle();
}

MainMenu::~MainMenu() {
}

void
MainMenu::Beginscreen() {
	std::cout << "MainMenu::MainMenu \n ";
	std::cout << std::endl << std::endl << std::endl << std::endl;
	std::cout << " \t\t\t\t ===============================================================" << std::endl;
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t World Racing \t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t Formula 1 \t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t|| \t\t\t\t\t\t\t\t||\n";
	std::cout << " \t\t\t\t ===============================================================" << std::endl;
	_getch();
	system("cls");
}

void
MainMenu::Handle() {
	std::cout << "MainManu::State " << my_fsm.GetState() << "\n ";
	do {
		switch (my_fsm.GetState()) {
		case GameFSM::MAINMENU:
			MainSceen();
			std::cin >> myinput_;
			switch (myinput_) {
			case 1:
				my_fsm.SetState(GameFSM::HISTORY);
				break;
			case 2:
				my_fsm.SetState(GameFSM::STATISTICS);
				break;
			case 3:
				my_fsm.SetState(GameFSM::PLAY);
				break;
			case 4:
				my_fsm.SetState(GameFSM::EXIT);
				break;
			default:
				my_fsm.SetState(GameFSM::MAINMENU);
			}
			Handle();
			break;
		case GameFSM::PLAY:
			std::cout << " 3. Begin game \n ";
			Play();
			Sleep(5);
			my_fsm.SetState(GameFSM::MAINMENU);
			Handle();
			break;
		case GameFSM::STATISTICS:
			std::cout << " 2. View statistics \n ";
			Sleep(5);
			my_fsm.SetState(GameFSM::MAINMENU);
			Handle();
			break;
		case GameFSM::HISTORY:
			SecondSceen();
			my_fsm.SetState(GameFSM::MAINMENU);
			Handle();
			break;
		case GameFSM::EXIT:
			std::cout << " 4. Exit game \n ";
			Sleep(5);
			exit(0);
		default:
			std::cout << " NO VALID CHOICE \n ";
			Sleep(5);
			my_fsm.SetState(GameFSM::MAINMENU);
			Handle();
		}
		_getch();
		system("cls");
	} while (true);
}

void
MainMenu::MainSceen() {
	std::cout << "  Main menu: \n ";
	std::cout << " 1. The most popular racer \n ";
	std::cout << " 2. View statistics \n ";
	std::cout << " 3. Begin game \n ";
	std::cout << " 4. Exit game \n ";
	std::cout << " \n\n ";
	Sleep(5);

}

void
MainMenu::SecondSceen() {
	std::cout << " 1. The most popular racer \n ";
	std::cout << " Alain Prost - world championships for McLaren and Williams. \n ";
	std::cout << " Ayrton Senna - F1 world championships for McLaren. \n ";
	std::cout << " Jackie Stewart - three world championships for Matra and Tyrrell \n ";
	std::cout << " Michael Schumacher - won the world championship for Benetton, and then won five times in a row with Ferrari \n ";
	std::cout << " Sebastian Vettel - won four world championships \n ";
	std::cout << " Jim Clark - was F1 world champion twice for Lotus \n ";
	std::cout << " Juan-Manuel Fangio - five world championships for four different teams \n ";
	Sleep(5);
	_getch();
	system("cls");
}

void
MainMenu::clearscreen() {
	HANDLE hOut;
	COORD Position;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	Position.X = 0;
	Position.Y = 0;
	SetConsoleCursorPosition(hOut, Position);
}

void
MainMenu::Play() {
	system("cls");
	char map[25][25];

	//loads the map 
	//with spaces and borders
	for (int i = 0; i < 20; ++i) {
		for (int j = 0; j < 20; ++j) {
			map[i][0] = '|';
			map[i][18] = '|';
			map[i][j] = ' ';
		}
	}

	int y = 18, x = 9; //the cars coordinates 
	srand(time(0));
	int a = 0, b = rand() % 15 + 2; //the obstacles coordiantes
	int speed = 100; //determines the speed of the obstacles (and the car) 
	bool startup = true;

	char obstacle = 219;

	char car = 'X';
	map[y][x] = car;
	//the game loop
	for (;;) {
		clearscreen();
		//places the car at its default location 
		map[y][x] = 'X';
		map[y][x + 1] = '|';
		map[y][x - 1] = '|';
		map[y + 1][x - 1] = 'o';
		map[y + 1][x + 1] = 'o';
		map[y - 1][x - 1] = 'o';
		map[y - 1][x + 1] = 'o';

		//generates the obstacles 
		map[a][b] = ' ';
		map[a][b] = ' ';
		map[a][b + 1] = ' ';
		map[a][b - 1] = ' ';
		map[a + 1][b - 1] = ' ';
		map[a + 1][b + 1] = ' ';
		map[a - 1][b - 1] = ' ';
		map[a - 1][b + 1] = ' ';
		++a;
		map[a][b] = obstacle;
		map[a][b + 1] = obstacle;
		map[a][b - 1] = obstacle;
		map[a + 1][b - 1] = obstacle;
		map[a + 1][b + 1] = obstacle;
		map[a - 1][b - 1] = obstacle;
		map[a - 1][b + 1] = obstacle;
		if (a > 20) {
			a = 0;
			b = rand() % 15 + 2;
		}
		//displays the map 
		for (int i = 0; i < 20; ++i) {
			for (int j = 0; j < 20; ++j) {
				std::cout << map[i][j];
				if (j >= 19) {
					std::cout << std::endl;
				}
			}
		}
		//does so the game starts 
		//after a key is pressed 
		if (startup) {
			_getch();
			startup = false;
		}

		//moves the car to the left 
		if (GetAsyncKeyState(VK_LEFT)) {
			if (map[y][x - 3] == obstacle) {
				goto lost;
			}
			else if (map[y][x - 3] != '|') {
				map[y][x] = ' ';
				map[y][x + 1] = ' ';
				map[y][x - 1] = ' ';
				map[y + 1][x - 1] = ' ';
				map[y + 1][x + 1] = ' ';
				map[y - 1][x - 1] = ' ';
				map[y - 1][x + 1] = ' ';
				x -= 3;
				map[y][x] = 'X';
				map[y][x + 1] = '|';
				map[y][x - 1] = '|';
				map[y + 1][x - 1] = 'o';
				map[y + 1][x + 1] = 'o';
				map[y - 1][x - 1] = 'o';
				map[y - 1][x + 1] = 'o';
			}
		}
		//moves the car to the right 
		if (GetAsyncKeyState(VK_RIGHT)) {
			if (map[y][x + 3] == obstacle) {
				goto lost;
			}
			else if (map[y][x + 3] != '|') {
				map[y][x] = ' ';
				map[y][x + 1] = ' ';
				map[y][x - 1] = ' ';
				map[y + 1][x - 1] = ' ';
				map[y + 1][x + 1] = ' ';
				map[y - 1][x - 1] = ' ';
				map[y - 1][x + 1] = ' ';
				x += 3;
				map[y][x] = 'X';
				map[y][x + 1] = '|';
				map[y][x - 1] = '|';
				map[y + 1][x - 1] = 'o';
				map[y + 1][x + 1] = 'o';
				map[y - 1][x - 1] = 'o';
				map[y - 1][x + 1] = 'o';
			}
		}
		//moves the car straight
		if (GetAsyncKeyState(VK_UP)) {
			if (map[y + 3][x] == obstacle) {
				goto lost;
			}
			else if (map[y + 3][x] != '|') {
				map[y][x] = ' ';
				map[y - 1][x] = ' ';
				map[y - 1][x] = ' ';
				map[y + 1][x - 1] = ' ';
				map[y + 1][x + 1] = ' ';
				map[y - 1][x - 1] = ' ';
				map[y - 1][x + 1] = ' ';
				y -= 3;
				map[y][x] = 'X';
				map[y][x - 1] = '|';
				map[y][x + 1] = '|';
				map[y - 1][x - 1] = 'o';
				map[y - 1][x + 1] = 'o';
				map[y + 1][x - 1] = 'o';
				map[y + 1][x + 1] = 'o';
			}
		}
		//moves the car backwards
		if (GetAsyncKeyState(VK_DOWN)) {
			if (map[y - 3][x] == obstacle) {
				goto lost;
			}
			else if (map[y - 3][x] != '|') {
				map[y][x] = ' ';
				map[y - 1][x] = ' ';
				map[y - 1][x] = ' ';
				map[y + 1][x - 1] = ' ';
				map[y + 1][x + 1] = ' ';
				map[y - 1][x - 1] = ' ';
				map[y - 1][x + 1] = ' ';
				y += 3;
				map[y][x] = 'X';
				map[y][x + 1] = '|';
				map[y][x - 1] = '|';
				map[y - 1][x - 1] = 'o';
				map[y - 1][x + 1] = 'o';
				map[y + 1][x - 1] = 'o';
				map[y + 1][x + 1] = 'o';
			}
		}

		//checks if the car crashed  
		if (map[y - 2][x] == obstacle || map[y - 2][x - 1] == obstacle || map[y - 2][x + 1] == obstacle) {
		lost:
			std::cout << "\n\nYou crashed!\n" << std::endl;
			std::cin.get();
			return;
		}

		Sleep(speed);
	}
}